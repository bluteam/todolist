<?php

  function delete_todo($file_name, $index){
    $todos = file($file_name);
    array_splice($todos, $delete_index, 1); 
    file_put_contents($file_name, $todos);
  }

  function show_list($file_name){
    $todos = file($file_name);
    $index = 0;
    foreach ($todos as $todo){
?>
      <li><?=$todo?><a href="todo.php?index=<?=$index?>">[delete]</a></li>
<?php
      $index++;
    }
  }

  $new_todo = $_POST["todo"];
  $delete_index = $_GET["index"];
  $TODO_LIST_FILE = "todolist.txt";
  if(isset($new_todo) && strlen(trim($new_todo)) != 0){
    file_put_contents($TODO_LIST_FILE, $new_todo."\n", FILE_APPEND);
  }
  if(isset($delete_index)){
    delete_todo($TODO_LIST_FILE, $delete_index);
  }
?>
<html>
<head>
</head>
<body>
  <form action="todo.php" method="POST">
    <input type="text" name="todo" size="20"/>
    <input type="submit" value="Add"/>
  </form>
  <ol>
<?php
  show_list($TODO_LIST_FILE);
?>
  </ol>
</body>
</html>
